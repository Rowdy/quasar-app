export default {
    computed: {
        logoUrl() {
            if (this.$q.dark.isActive) {
              return require('assets/logo_dark.png')
          } else {
              return require('assets/logo_light.png')
            }
      }
    }
}