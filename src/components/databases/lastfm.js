import axios from "axios"

var api_key = '7dca4940e102678124c1902ffb3165be'
var base_url = 'https://ws.audioscrobbler.com/2.0/'



async function fmGetCharts (
    limit = 10
) {
    const resp = await axios.get(`${base_url}?method=chart.gettoptracks&api_key=${api_key}&limit=${limit}&format=json`)
    let songs = JSON.parse(resp.request.response)
    songs = songs.tracks.track
    let result = []
    songs.forEach(song => {
            result.push(
                {
                    title: song.name,
                    artist: song.artist.name,
                    cover_image: song.image[0]["#text"]
                }
            )
    });
    return result
}

async function fmGetSimilar (
    artist,
    track
) {
    const resp = await axios.get(`${base_url}?method=track.getsimilar&artist=${artist}&track=${track}&limit=20&api_key=${api_key}&format=json`)
    let similarTracks = resp.data.similartracks.track
    let filteredSimilarTracks = []
    similarTracks.forEach(track => {
        filteredSimilarTracks.push(
            {
                artist: track.artist.name,
                title: track.name,
                image: track.image[3]['#text'],
            }
        )
    });
    return filteredSimilarTracks
}

async function fmGetTrackInfo (
    track,
    artist
) {
    const resp = await axios.get(`${base_url}?method=track.getInfo&api_key=${api_key}&track=${track}&artist=${artist}&format=json`)
    console.log(resp)
}

export { fmGetCharts, fmGetSimilar }