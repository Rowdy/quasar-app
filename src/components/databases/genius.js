var api_headers = {
    "x-rapidapi-host": "genius.p.rapidapi.com",
    "x-rapidapi-key": "8827ccd331msh2972185b126e7e2p1db29ejsna1c7184eca1c"
};

async function geniusSearch (
    query
    ) {
        let filteredSongs = []
        const resp = await fetch(`https://genius.p.rapidapi.com/search?q=${query}`, {
          "method": "GET",
          "headers": api_headers
        })
        const songs = await resp.json();
        songs.response.hits.forEach(song => {
            filteredSongs.push({
              label: song.result.primary_artist.name + " - " + song.result.title,
              value: song.result.id,

              id: song.result.id, 
              artist: song.result.primary_artist.name,
              title: song.result.title,
              album: song.result.album,
              cover_image: song.result.song_art_image_thumbnail_url,
            })
        })
        return filteredSongs
}

async function geniusGetSong (
    id
) 
{
    const resp = await fetch(`https://genius.p.rapidapi.com/songs/${id}`, {
        "method": "GET",
        "headers": api_headers
    })

    const songResp = await resp.json();
    let song = songResp.response.song;
    return {
        id: song.id,
        artist: song.primary_artist.name,
        title: song.title,
        album: song.album,
        path: song.path,
        platforms: song.media,
        cover_image: song.song_art_image_url,
        lyrics: null,
    }
    
}

async function geniusGetLyrics (
    path
)   {
        const resp = await fetch(`https://cors-anywhere.herokuapp.com/https://genius.com${path}`, {
                "method": "GET",
            })
            const data = await resp.text()
            let lyrics = document.createElement('html')
            lyrics.innerHTML = data;
            lyrics = lyrics.querySelector('.lyrics').innerHTML
            lyrics = lyrics.replace(/(<p>|<\/p>)/g, '\n')
            lyrics = lyrics.replace(/(<([^>]+)>)/ig, '')
            lyrics = lyrics.trim()
            lyrics = lyrics.replace(/(?:\r\n|\r|\n)/g, '<br>')
            lyrics = lyrics.split('<br><br>')

            let filteredLyrics = []
            lyrics.forEach((item, index) => {
                filteredLyrics.push({
                    id: index,
                    lyric: item,
                    translatedLyric: ''
                })
            });
            return filteredLyrics
}

export { geniusSearch, geniusGetSong, geniusGetLyrics }