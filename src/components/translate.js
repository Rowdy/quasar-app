import axios from "axios"

var api_key = "trnsl.1.1.20200402T170819Z.5f49e961c9401a4f.b405e472dbe4d297e69f7bc25bfaee318aeac5d2"
var base_url = "https://translate.yandex.net/api/v1.5/tr.json/"

async function translateLyrics(
    lyric,
    lang
) {
    let trn = lyric.replace(/(<br>|<br>)/g, ' <br> ')
    const resp = await axios.get(`${base_url}translate?text=${trn}&format=html&lang=${lang}&key=${api_key}`)
    return capitilize(resp.data.text[0])
}

async function detectLanguage(
    text
) {
    const resp = await axios.get(`${base_url}detect?text=${text}&key=${api_key}`)
    if(resp.data.lang == "ru") return "en"
    else return "ru"
}

function capitilize(
    text
) {
        return text.replace(/(^| <br> )\S/g, function(a) {return a.toUpperCase()})
}

export { translateLyrics, detectLanguage }